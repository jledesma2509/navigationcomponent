package com.academiamoviles.kotlinwebinarjunio.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Persona(
    val nombres:String,
    val apellidos:String) : Parcelable {
}