package com.academiamoviles.kotlinwebinarjunio.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.academiamoviles.kotlinwebinarjunio.R
import kotlinx.android.synthetic.main.fragment_segundo.*


class SegundoFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_segundo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /*
        arguments?.let{
            val nombres = it.getString("nombres","")
            tvNombresDestino.text = nombres
        }
         */
        arguments?.let {
            //Using SafeArgs
            val nombres = SegundoFragmentArgs.fromBundle(it).nombresSafe
            val apellidos = SegundoFragmentArgs.fromBundle(it).apellidosSafe
            val persona = SegundoFragmentArgs.fromBundle(it).ClassPerson
            //tvNombresDestino.text = "$nombres $apellidos"
            tvNombresDestino.text = "${persona.nombres} ${persona.apellidos}"
        }
    }
}