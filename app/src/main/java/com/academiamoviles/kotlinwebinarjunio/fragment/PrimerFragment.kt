package com.academiamoviles.kotlinwebinarjunio.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.academiamoviles.kotlinwebinarjunio.R
import com.academiamoviles.kotlinwebinarjunio.model.Persona
import kotlinx.android.synthetic.main.fragment_primer.*


class PrimerFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_primer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnEnviar?.setOnClickListener {

            val nombres = edtNombres.text.toString()
            val apellidos = edtApellidos.text.toString()

            /* OPCION CON BUNDLE
            val bundle = Bundle()
            bundle.putString("nombres", nombres)
            Navigation.findNavController(it).navigate(R.id.action_primerFragment_to_segundoFragment,bundle)
            */

            val persona = Persona(nombres,apellidos)

            // OPCION CON SAFE ARGS
            //val directions = PrimerFragmentDirections.actionPrimerFragmentToSegundoFragment(nombresSafe = nombres, apellidosSafe = apellidos )
            //Navigation.findNavController(it).navigate(directions)

            val directions = PrimerFragmentDirections.actionPrimerFragmentToSegundoFragment(nombres,apellidos,persona)
            Navigation.findNavController(it).navigate(directions)


        }
    }


}